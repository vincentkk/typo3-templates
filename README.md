
Installation
--------------
  * First install ddev if it's not present: https://ddev.readthedocs.io/en/stable/
  * Install also yarn
  * Generate assets, run "yarn install" and next "gulp"
  * Once done run "ddev start"

Users
--------------
 * Backend admin: admin/password
